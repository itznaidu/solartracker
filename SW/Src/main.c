/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Input_Aggregator_api.h"
#include "stdio.h"
#include "stdbool.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
Type_Input_Data Input_Data;

uint32_t RunTime;
char Display_buffer[100];


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM5_Init();
  MX_ADC1_Init();
  MX_I2C3_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	uint32_t RuntimeMeas_Start,RuntimMeas_End = 0;
	Input_Data.StateMachine = InitPhase;
	Init_Input_Devices();
	char buffer[34];
	int flag=5; //thingspeak schedule buffer
	while (1)
	{
		RuntimeMeas_Start = HAL_GetTick();
		if(LIMIT_SD != GPIO_PIN_SET)
		{
			UP_LINEAR_CA
			UP_LINEAR_CC
			UP_LINEAR_CD //GOOD couple
			UP_LINEAR_SA
			UP_LINEAR_SB
			UP_LINEAR_SC
			UP_LINEAR_SD

		}
		else
		{
			DOWN_LINEAR_CA
			DOWN_LINEAR_CC
			DOWN_LINEAR_CD
			DOWN_LINEAR_SA
			DOWN_LINEAR_SB
			DOWN_LINEAR_SC
			DOWN_LINEAR_SD
		}
//
//		HAL_Delay(35000);
//
//		HAL_Delay(35000);
//		STOP_LINEAR_CA
		if (flag == 0)
		{
			UP_LINEAR_CA
			UP_LINEAR_CB
			UP_LINEAR_CC
			UP_LINEAR_CD

			if(LIMIT_CA == GPIO_PIN_SET)
			{
				STOP_LINEAR_CA
			}
			else if(LIMIT_CB == GPIO_PIN_SET)
			{
				STOP_LINEAR_CB
			}
			else if(LIMIT_CC == GPIO_PIN_SET)
			{
				STOP_LINEAR_CC
			}
			else if(LIMIT_CD == GPIO_PIN_SET)
			{
				STOP_LINEAR_CD
			}

			if( (LIMIT_CA  == GPIO_PIN_SET) &&
					(LIMIT_CB  == GPIO_PIN_SET) &&
					(LIMIT_CC  == GPIO_PIN_SET) &&
					(LIMIT_CD  == GPIO_PIN_SET) )
			{
				STOP_LINEAR_CA
				STOP_LINEAR_CB
				STOP_LINEAR_CC
				STOP_LINEAR_CD
				flag = 1;
			}
		}
		else if(flag == 1)
		{
			UP_LINEAR_SA
			UP_LINEAR_SB
			UP_LINEAR_SC
			UP_LINEAR_SD
			//same as before
			if(LIMIT_SA == GPIO_PIN_SET)
			{
				STOP_LINEAR_SA
			}
			else if(LIMIT_SB == GPIO_PIN_SET)
			{
				STOP_LINEAR_SB
			}
			else if(LIMIT_SC == GPIO_PIN_SET)
			{
				STOP_LINEAR_SC
			}
			else if(LIMIT_SD == GPIO_PIN_SET)
			{
				STOP_LINEAR_SD
			}

			if( (LIMIT_SA  == GPIO_PIN_SET) &&
					(LIMIT_SB  == GPIO_PIN_SET) &&
					(LIMIT_SC  == GPIO_PIN_SET) &&
					(LIMIT_SD  == GPIO_PIN_SET) )
			{
				STOP_LINEAR_SA
				STOP_LINEAR_SB
				STOP_LINEAR_SC
				STOP_LINEAR_SD
				flag = 2;
			}


		}
		else if (flag == 3)
		{
			DOWN_LINEAR_SA
			DOWN_LINEAR_SB
			DOWN_LINEAR_SC
			DOWN_LINEAR_SD
			HAL_Delay(35000);
			{
				STOP_LINEAR_SA
				STOP_LINEAR_SB
				STOP_LINEAR_SC
				STOP_LINEAR_SD

			}
			flag = 4;

		}
		else if (flag ==4)
		{
			DOWN_LINEAR_CA
			DOWN_LINEAR_CB
			DOWN_LINEAR_CC
			HAL_Delay(35000);
			{
				STOP_LINEAR_CA
				STOP_LINEAR_CB
				STOP_LINEAR_CC
				STOP_LINEAR_CD

			}
		}
		//Don't touch
		RuntimMeas_End = HAL_GetTick();
		RunTime = RuntimMeas_End-RuntimeMeas_Start;

		//MASTER RESET FROM USER
		if(RESET == GPIO_PIN_SET)
		{
			Input_Data.StateMachine = Reset;
		}



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 160;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void Vibration_Alert()
{
	Input_Data.StateMachine = Emergency_Stop;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
