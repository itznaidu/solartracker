/*
 * Defines.h
 *
 *  Created on: 01-Jul-2020
 *      Author: Varma
 */

#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#ifndef USER_CODE_DEFINES_H_
#define USER_CODE_DEFINES_H_

#define PWM_90         __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_1, 720);
#define PWM_50         __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_1, 400);
#define PWM_00         __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_1, 001);

#define FRONT_NEAR     HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_8)
#define FRONT_FAR      HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_10)

#define SIDE           HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_8)

#define REAR_NEAR      HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_10)
#define REAR_FAR       HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_9)


#define BASE_FORWARD   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET );
#define BASE_REVERSE   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET   );
#define BASE_STOP      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET   );

#define UP_LINEAR_CA   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET );
#define DOWN_LINEAR_CA HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET   );
#define STOP_LINEAR_CA HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET   );

#define UP_LINEAR_CB   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1,  GPIO_PIN_RESET );
#define DOWN_LINEAR_CB HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1,  GPIO_PIN_SET   );
#define STOP_LINEAR_CB HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1,  GPIO_PIN_SET   );

#define UP_LINEAR_CC   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3,  GPIO_PIN_RESET );
#define DOWN_LINEAR_CC HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3,  GPIO_PIN_SET   );
#define STOP_LINEAR_CC HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3,  GPIO_PIN_SET   );

#define UP_LINEAR_CD   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,  GPIO_PIN_RESET );
#define DOWN_LINEAR_CD HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,  GPIO_PIN_SET   );
#define STOP_LINEAR_CD HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,  GPIO_PIN_SET   );

#define UP_LINEAR_SA   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_1,  GPIO_PIN_RESET );
#define DOWN_LINEAR_SA HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_1,  GPIO_PIN_SET   );
#define STOP_LINEAR_SA HAL_GPIO_WritePin(GPIOE, GPIO_PIN_0,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_1,  GPIO_PIN_SET   );

#define UP_LINEAR_SB   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3,  GPIO_PIN_RESET );
#define DOWN_LINEAR_SB HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3,  GPIO_PIN_SET   );
#define STOP_LINEAR_SB HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3,  GPIO_PIN_SET   );

#define UP_LINEAR_SC   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET );
#define DOWN_LINEAR_SC HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET   );
#define STOP_LINEAR_SC HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET   );

#define UP_LINEAR_SD   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5,  GPIO_PIN_RESET );
#define DOWN_LINEAR_SD HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,  GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5,  GPIO_PIN_SET   );
#define STOP_LINEAR_SD HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4,  GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5,  GPIO_PIN_SET   );

#define LIMIT_CA       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_15)
#define LIMIT_CB       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_14)
#define LIMIT_CC       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_11)
#define LIMIT_CD       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_10)

#define LIMIT_SA       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_9)
#define LIMIT_SB       HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_8)
#define LIMIT_SC       HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_15)
#define LIMIT_SD       HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_14)

#define RACK_EXT_OUT   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET );
#define RACK_EXT_IN    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET   );
#define RACK_EXT_STOP  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET   );\
	                   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET   );

#define RACK_DELAY     5000
#define LINEAR_S_DELAY 5000
#define LINEAR_C_DELAY 5000
#define RESET_DELAY    7000

#define RESET          HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_7)



#endif /* USER_CODE_DEFINES_H_ */
