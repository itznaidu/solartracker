/*
 * StateMachine.c
 *
 *  Created on: 01-Jul-2020
 *      Author: Varma
 */
#include "Input_Aggregator_api.h"
#include "string.h"

uint32_t Rack_Extend_Delay       = 0;
uint32_t Rack_Extend_StartTime   = 0;
uint32_t Linear_Extend_Delay     = 0;
uint32_t Linear_Extend_StartTime = 0;
uint32_t Slow_Flag ;  //0-No Detect (init) : 1-first detect : 2-between first and second detect : 3-Second detect
bool Front_Clear;
bool Buddy_Active;


void Execute_State_Machine (Type_Input_Data *Input_Data)
{
	switch (Input_Data->StateMachine)
	{
	case InitPhase:
		Front_Clear  = false;
		Buddy_Active = false;
		Slow_Flag    = 0;

		Init_Input_Devices();
		Input_Data->GPS_Location.Active = Get_GPS_Data(&(Input_Data->GPS_Location));

		if( (FRONT_NEAR == GPIO_PIN_RESET) && (FRONT_FAR == GPIO_PIN_RESET) )
		{
			Front_Clear = true;
		}
		//if(Check if Buddy is initialized properly. Will be communicating via RF. Yet to implement)
		{
			Buddy_Active = true;
		}
		//All good. Good to next statemachine
		if( Input_Data->GPS_Location.Active && Front_Clear && Buddy_Active)
		{
			Input_Data->StateMachine = WaitForStart;
		}
		break;
	case WaitForStart: //STEP-1
		//Check if time matches
		if(!(strcmp(Input_Data->GPS_Location.Time,"050000"))) //when time matches
		{
			Input_Data->StateMachine = DriveForward;
		}
		break;
	case DriveForward: //STEP-2-3-4
		if(Slow_Flag == 0)
		{
			PWM_90
			BASE_FORWARD
		}

		if( ( (SIDE == GPIO_PIN_SET) && (Slow_Flag == 0) ) || (FRONT_FAR == GPIO_PIN_SET) )
		{//First detect. Either put a delay here for debounce logic or second optionss
			PWM_50
			Slow_Flag = 1;
		}
		else if( ( (SIDE == GPIO_PIN_RESET) && (Slow_Flag == 1) ) || (FRONT_FAR == GPIO_PIN_SET) )
		{//First detect passed now yet to reach second plate
			PWM_50
			Slow_Flag = 2;
		}
		else if( (SIDE == GPIO_PIN_SET) && (Slow_Flag == 2) )
		{//First detect then no-detect then detect. This loop will be entered
			BASE_STOP
			PWM_00
			{
				Slow_Flag    = 0;
				Input_Data->StateMachine = RackExtend;
			}
		}
		else if ( (FRONT_FAR == GPIO_PIN_SET) && (FRONT_NEAR == GPIO_PIN_SET) )
		{
			BASE_STOP
			PWM_00
			Input_Data->StateMachine = DriveHome;
		}
		Rack_Extend_StartTime = HAL_GetTick();
		Rack_Extend_Delay     = RACK_DELAY;
		break;
	case RackExtend: //STEP-5
		RACK_EXT_OUT
		if(Rack_Extend_StartTime > HAL_GetTick()) //NEED REVIEW _ TO COMPENSATE OVERFLOW __ COULD BE SIMPLER
		{
			Rack_Extend_Delay     = RACK_DELAY - (0xFFFFFFFF - Rack_Extend_StartTime);
			Rack_Extend_StartTime = HAL_GetTick();
		}
		else if(HAL_GetTick()> (Rack_Extend_Delay + Rack_Extend_StartTime))
		{
			RACK_EXT_STOP
			Input_Data->StateMachine = LiftUp_C;
		}
		break;
	case LiftUp_C: //STEP-6-7
		UP_LINEAR_CA
		UP_LINEAR_CB
		UP_LINEAR_CC
		UP_LINEAR_CD

		if(LIMIT_CA == GPIO_PIN_SET)
		{
			STOP_LINEAR_CA
		}
		else if(LIMIT_CB == GPIO_PIN_SET)
		{
			STOP_LINEAR_CB
		}
		else if(LIMIT_CC == GPIO_PIN_SET)
		{
			STOP_LINEAR_CC
		}
		else if(LIMIT_CD == GPIO_PIN_SET)
		{
			STOP_LINEAR_CD
		}

		if( (LIMIT_CA  == GPIO_PIN_SET) &&
		    (LIMIT_CB  == GPIO_PIN_SET) &&
			(LIMIT_CC  == GPIO_PIN_SET) &&
			(LIMIT_CD  == GPIO_PIN_SET) )
		{
			Input_Data->StateMachine = LiftUp_S;
			STOP_LINEAR_CA
			STOP_LINEAR_CB
			STOP_LINEAR_CC
			STOP_LINEAR_CD
		}
		break;
	case LiftUp_S: //STEP 8 - 9
		UP_LINEAR_SA
		UP_LINEAR_SB
		UP_LINEAR_SC
		UP_LINEAR_SD
		//same as before
		if(LIMIT_SA == GPIO_PIN_SET)
		{
			STOP_LINEAR_SA
		}
		else if(LIMIT_SB == GPIO_PIN_SET)
		{
			STOP_LINEAR_SB
		}
		else if(LIMIT_SC == GPIO_PIN_SET)
		{
			STOP_LINEAR_SC
		}
		else if(LIMIT_SD == GPIO_PIN_SET)
		{
			STOP_LINEAR_SD
		}

		if( (LIMIT_SA  == GPIO_PIN_SET) &&
		    (LIMIT_SB  == GPIO_PIN_SET) &&
			(LIMIT_SC  == GPIO_PIN_SET) &&
			(LIMIT_SD  == GPIO_PIN_SET) )
		{
			Input_Data->StateMachine = DeployBuddy;
			STOP_LINEAR_SA
			STOP_LINEAR_SB
			STOP_LINEAR_SC
			STOP_LINEAR_SD
		}
		break;
	case DeployBuddy: //STEP-9
		//send message on NRF TO GO
		//solarbuddy deployed
		Input_Data->StateMachine = WaitForBuddy;
		break;
	case WaitForBuddy: //STEP-10
		//wait for the NRF signal from buddy that it has arrived
		//if buddy arrives
		Input_Data->StateMachine = RackRetrive;
		Rack_Extend_StartTime = HAL_GetTick();
		Rack_Extend_Delay     = RACK_DELAY + 1000; //Add a thousand offset to make sure it is completely retrevied back
		break;
	case RackRetrive: //STEP-11
		RACK_EXT_IN
		if(Rack_Extend_StartTime > HAL_GetTick()) //NEED REVIEW _ TO COMPENSATE OVERFLOW __ COULD BE SIMPLER
		{
			Rack_Extend_Delay     = RACK_DELAY - (0xFFFFFFFF - Rack_Extend_StartTime);
			Rack_Extend_StartTime = HAL_GetTick();
		}
		else if(HAL_GetTick()> (Rack_Extend_Delay + Rack_Extend_StartTime))
		{
			RACK_EXT_STOP
			Input_Data->StateMachine = LowerDown_S;
		}
		Linear_Extend_StartTime = HAL_GetTick();
		Linear_Extend_Delay     = LINEAR_S_DELAY + 1000;
		break;
	case LowerDown_S: //STEP-12
		DOWN_LINEAR_SA
		DOWN_LINEAR_SB
		DOWN_LINEAR_SC
		DOWN_LINEAR_SD
		if(Linear_Extend_StartTime > HAL_GetTick()) //NEED REVIEW _ TO COMPENSATE OVERFLOW __ COULD BE SIMPLER
		{
			Linear_Extend_Delay     = LINEAR_S_DELAY - (0xFFFFFFFF - Linear_Extend_StartTime);
			Linear_Extend_StartTime = HAL_GetTick();
		}
		else if(HAL_GetTick()> (Linear_Extend_Delay + Linear_Extend_StartTime))
		{
			STOP_LINEAR_SA
			STOP_LINEAR_SB
			STOP_LINEAR_SC
			STOP_LINEAR_SD
			Input_Data->StateMachine = LowerDown_C;
			Linear_Extend_StartTime = HAL_GetTick();
			Linear_Extend_Delay     = LINEAR_C_DELAY + 1000;
		}
		break;
	case LowerDown_C:
		DOWN_LINEAR_CA
		DOWN_LINEAR_CB
		DOWN_LINEAR_CC
		DOWN_LINEAR_CD
		if(Linear_Extend_StartTime > HAL_GetTick()) //NEED REVIEW _ TO COMPENSATE OVERFLOW __ COULD BE SIMPLER
		{
			Linear_Extend_Delay     = LINEAR_C_DELAY - (0xFFFFFFFF - Linear_Extend_StartTime);
			Linear_Extend_StartTime = HAL_GetTick();
		}
		else if(HAL_GetTick()> (Linear_Extend_Delay + Linear_Extend_StartTime))
		{
			STOP_LINEAR_CA
			STOP_LINEAR_CB
			STOP_LINEAR_CC
			STOP_LINEAR_CD
			Input_Data->StateMachine = DriveForward;
		}
		break;
	case DriveHome:
		BASE_REVERSE
		PWM_90
		if( (REAR_NEAR == GPIO_PIN_SET) && (REAR_FAR == GPIO_PIN_SET) )
		{
			PWM_00
			BASE_STOP
			Input_Data->StateMachine = WaitForStart;
		}
		else if (REAR_FAR == GPIO_PIN_SET)
		{
			PWM_50
		}
		break;
	case Reset: //NOTE: RESET SHOULD BE PRESSED ONLY WHEN SOLAR BUDDY IS HOME
		BASE_STOP
		DOWN_LINEAR_SA
		DOWN_LINEAR_SB
		DOWN_LINEAR_SC
		DOWN_LINEAR_SD
		DOWN_LINEAR_CA
		DOWN_LINEAR_CB
		DOWN_LINEAR_CC
		DOWN_LINEAR_CD
		RACK_EXT_IN
		HAL_Delay(RESET_DELAY);
		STOP_LINEAR_SA
		STOP_LINEAR_SB
		STOP_LINEAR_SC
		STOP_LINEAR_SD
		STOP_LINEAR_CA
		STOP_LINEAR_CB
		STOP_LINEAR_CC
		STOP_LINEAR_CD
		RACK_EXT_STOP
		BASE_REVERSE
		PWM_90
		Input_Data->StateMachine = DriveHome;
		break;
	case Emergency_Stop:
		STOP_LINEAR_SA
		STOP_LINEAR_SB
		STOP_LINEAR_SC
		STOP_LINEAR_SD
		STOP_LINEAR_CA
		STOP_LINEAR_CB
		STOP_LINEAR_CC
		STOP_LINEAR_CD
		RACK_EXT_STOP
		BASE_STOP
		break;

	}
	return;
}
