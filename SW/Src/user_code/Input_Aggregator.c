/*
 * Input_Aggregator.c
 *
 *  Created on: 21-Jun-2020
 *      Author: Varma
 */

#include "main.h"
#include "usart.h"
#include "string.h"
#include "tim.h"
#include "i2c.h"
#include "adc.h"
#include "Input_Aggregator_api.h"

uint8_t GPS_BUFFER[52];
uint8_t I2C_Scratch[0];
uint8_t i2c_buffer[14];
uint16_t adc_buffer[14];
HAL_StatusTypeDef Return_Value;
void GPS_USART3_RESET(void)
{
	/*
	 * This function is called from the IDLE DETECT ISR
	 * of USART3 to reset the DMA BUFFER
	 */
	HAL_NVIC_EnableIRQ(USART3_IRQn);
	__HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
	HAL_UART_Receive_DMA(&huart3,GPS_BUFFER,52);
}
void Init_I2C_IMU (uint16_t Address)
{
    //I2C INIT
    //check if I2C is okay
    Return_Value = HAL_I2C_IsDeviceReady(&hi2c1,Address , 2, 5);
    //if it returns 0x68 then all okay
	HAL_I2C_Mem_Read (&hi2c1, Address,0x75,1, (uint8_t *)I2C_Scratch, 1, 1000);
	//Send zero to wakeup the IC more info on DataSheet. 0x6B PWR_MGMT_1 Register
	I2C_Scratch[0] = 0;
	HAL_I2C_Mem_Write(&hi2c1, Address,0x6B, 1,(uint8_t *)I2C_Scratch, 1, 1000);
	//Set Accelerometer Sensitivity.0x1C ACCEL_CONFIG
	HAL_I2C_Mem_Write(&hi2c1, Address,0x1C, 1,(uint8_t *)I2C_Scratch, 1, 1000);
	//Set Gyroscope sensitivity. 0x1B GYRO_CONFIG
	HAL_I2C_Mem_Write(&hi2c1, Address,0x1B, 1,(uint8_t *)I2C_Scratch, 1, 1000);
	//set gyro update rate. 0x19 SMPLRT_DIV
	I2C_Scratch[0] = 0x07;
	HAL_I2C_Mem_Write(&hi2c1, Address,0x19, 1,(uint8_t *)I2C_Scratch, 1, 1000);
}

void Init_Input_Devices()
{
	/*UART3 FOR GPS INIT*/
	char GGA[11] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xf0,0x00,0x00,0xfa,0x0f};
	char GSA[11] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xf0,0x02,0x00,0xfc,0x13};
	char GSV[11] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xf0,0x03,0x00,0xfd,0x15};
	char RMC[11] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xf0,0x04,0x00,0xfe,0x17};
	char VTG[11] = {0xB5,0x62,0x06,0x01,0x03,0x00,0xf0,0x05,0x00,0xff,0x19};
	HAL_UART_Transmit(&huart3, (uint8_t *)GGA, 11,111 );
	HAL_UART_Transmit(&huart3, (uint8_t *)GSA, 11,111 );
	HAL_UART_Transmit(&huart3, (uint8_t *)GSV, 11,111 );
	HAL_UART_Transmit(&huart3, (uint8_t *)RMC, 11,111 );
	HAL_UART_Transmit(&huart3, (uint8_t *)VTG, 11,111 );
	GPS_USART3_RESET();

	/*ENCODER*/       //CH:A-CH:B
	TIM5->CR1 = 0x01; //PA00-PA01  : Encoder1
	TIM2->CR1 = 0x01; //PB03-PA15  : Encoder2
	TIM3->CR1 = 0x01; //PC07-PC06  : Encoder3
	TIM4->CR1 = 0x01; //PD13-PD12  : Encoder4

	/* PWM USING TIMER 1  TIMER VALUE 0-800*/
	HAL_TIM_Base_Start_IT(&htim1);
    HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);

    //I2C INIT
    Init_I2C_IMU(0xD0);
    Init_I2C_IMU(0xD2);

    //ADC_INIT
    HAL_ADC_Start(&hadc1);
    HAL_ADC_Start_DMA(&hadc1,(uint32_t*)adc_buffer,14);

}



bool Get_GPS_Data(Type_GPS *local_struct)
{
	bool status = false;
	/*
	$GNGLL,1258.75834,N,07733.27846,E,152856.00,A,A*76
	FrameFormat
	1258.75834 is LAT at 7th Index size 10
	07733.27846 is LONG 20th Index size 11
	152856 is TIME in UTC at 34th Index size 6
	Size is fixed as per the protocol
	 */
	if( (GPS_BUFFER[0] == '$') && (GPS_BUFFER[11] == '.') && (GPS_BUFFER[50] == '\r') && (GPS_BUFFER[47] == '*') )
	{
		memcpy((local_struct->Lat),&GPS_BUFFER[7],10);
		memcpy((local_struct->Long),&GPS_BUFFER[20],11);
		memcpy((local_struct->Time),&GPS_BUFFER[34],6);
		status = true;
	}
	return status;
}
void Get_IMU_Data(uint16_t *Gyro_data, uint16_t Address)
{
	HAL_I2C_Mem_Read(&hi2c1, Address,0x3B, 1, (uint8_t *)i2c_buffer, 14, 1000);

	Gyro_data[0] = i2c_buffer[0]  << 8 | i2c_buffer[1];
	Gyro_data[1] = i2c_buffer[2]  << 8 | i2c_buffer[3];
	Gyro_data[2] = i2c_buffer[4]  << 8 | i2c_buffer[5];
	Gyro_data[3] = i2c_buffer[8]  << 8 | i2c_buffer[9];
	Gyro_data[4] = i2c_buffer[10] << 8 | i2c_buffer[11];
	Gyro_data[5] = i2c_buffer[12] << 8 | i2c_buffer[13];
}

void Get_LIMIT_State(Type_Input_Data *InputData)
{
	InputData->Couple_LimitSwitch[0] = LIMIT_CA;
	InputData->Couple_LimitSwitch[1] = LIMIT_CB;
	InputData->Couple_LimitSwitch[2] = LIMIT_CC;
	InputData->Couple_LimitSwitch[3] = LIMIT_CD;

	InputData->Single_LimitSwitch[0] = LIMIT_SA;
	InputData->Single_LimitSwitch[1] = LIMIT_SB;
	InputData->Single_LimitSwitch[2] = LIMIT_SC;
	InputData->Single_LimitSwitch[3] = LIMIT_SD;

}

void Get_IR_State(Type_Input_Data *InputData)
{
	InputData->Front_IR[0] = FRONT_NEAR;
	InputData->Front_IR[1] = FRONT_FAR;

	InputData->Rear_IR[0]  = REAR_NEAR;
	InputData->Rear_IR[1]  = REAR_FAR;

	InputData->Side_IR[0]  = SIDE;

}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if( (GPIO_Pin == GPIO_PIN_12) || (GPIO_Pin == GPIO_PIN_13))
	{
		Vibration_Alert();
	}
}

