/*
 * Input_Aggregator_api.h
 *
 *  Created on: 21-Jun-2020
 *      Author: Varma
 */

#ifndef INPUT_AGGREGATOR_API_H_
#define INPUT_AGGREGATOR_API_H_

#include "Defines.h"

void Init_Input_Devices(void);
void Init_I2C_IMU (uint16_t Address);
void GPS_USART3_RESET(void);
bool Get_GPS_Data(Type_GPS *local_struct);
void Execute_State_Machine (Type_Input_Data *Input_Data);
void Get_IMU_Data(uint16_t *Gyro_data, uint16_t Address);
void Get_LIMIT_State(Type_Input_Data *InputData);
void Get_IR_State(Type_Input_Data *InputData);
void Vibration_Alert(void);

#endif /* INPUT_AGGREGATOR_API_H_ */


