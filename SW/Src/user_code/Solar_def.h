/*
 * Solar_def.h
 *
 *  Created on: 21-Jun-2020
 *      Author: Varma
 */

#ifndef SOLAR_DEF_H_
#define SOLAR_DEF_H_

#include "stdbool.h"

typedef struct Encoder_Data_Tag
{
	uint16_t Speed;
	uint8_t Direction; //CW-1 ACW-0

}Type_Encoder_Data;

typedef struct GPS_Tag
{
	uint8_t Lat[10];
	uint8_t Long[11];
	uint8_t Time[6];
	bool Active;
}Type_GPS;

typedef struct ADC_Tag
{
	uint16_t Lateral_Motor[4];
	uint16_t Paired_Linear_Motor[8];
	uint16_t Single_Linear_Motor[4];
	uint16_t Lineaar_Motor[8];
}Type_ADC;

typedef enum StateMachine_Tag
{
	InitPhase      = 0x00,
	WaitForStart   = 0x11,
	DriveForward   = 0x22,
	RackExtend     = 0x33,
	LiftUp_C       = 0x44,
	LiftUp_S       = 0x55,
	DeployBuddy    = 0x66,
	WaitForBuddy   = 0x77,
	RackRetrive    = 0x88,
	LowerDown_S    = 0x99,
	LowerDown_C    = 0xAA,
	DriveHome      = 0xBB,
	Reset          = 0xCC,
	Emergency_Stop = 0xDD
}Type_StateMachine;

typedef struct Input_Data_Tag
{
	GPIO_PinState Couple_LimitSwitch[4];
	GPIO_PinState Single_LimitSwitch[4];
	uint16_t Top_IMU[6]; //First 3 ACCL. Next 3 GYRO X Y Z
	uint16_t Bottom_IMU[6];
	GPIO_PinState Front_IR[2];
	GPIO_PinState Rear_IR[2];
	GPIO_PinState Side_IR[1];
	uint8_t Vibration[2];
	Type_Encoder_Data Encoder[4];
	Type_GPS GPS_Location;
	Type_ADC Motor_Current;
	Type_StateMachine StateMachine;

}Type_Input_Data;



#endif /* SOLAR_DEF_H_ */
