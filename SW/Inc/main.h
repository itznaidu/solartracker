/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Solar_def.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LINEAR_SB__Pin GPIO_PIN_2
#define LINEAR_SB__GPIO_Port GPIOE
#define LINEAR_SB_E3_Pin GPIO_PIN_3
#define LINEAR_SB_E3_GPIO_Port GPIOE
#define LINEAR_SD__Pin GPIO_PIN_4
#define LINEAR_SD__GPIO_Port GPIOE
#define LINEAR_SD_E5_Pin GPIO_PIN_5
#define LINEAR_SD_E5_GPIO_Port GPIOE
#define ENC_1A_Pin GPIO_PIN_0
#define ENC_1A_GPIO_Port GPIOA
#define ENC_1B_Pin GPIO_PIN_1
#define ENC_1B_GPIO_Port GPIOA
#define FRONT_IR_NEAR_Pin GPIO_PIN_8
#define FRONT_IR_NEAR_GPIO_Port GPIOE
#define PWM_MOTOR_Pin GPIO_PIN_9
#define PWM_MOTOR_GPIO_Port GPIOE
#define FRONT_IR_FAR_Pin GPIO_PIN_10
#define FRONT_IR_FAR_GPIO_Port GPIOE
#define VIBRATION_A_Pin GPIO_PIN_12
#define VIBRATION_A_GPIO_Port GPIOE
#define VIBRATION_A_EXTI_IRQn EXTI15_10_IRQn
#define VIBRATION_B_Pin GPIO_PIN_13
#define VIBRATION_B_GPIO_Port GPIOE
#define VIBRATION_B_EXTI_IRQn EXTI15_10_IRQn
#define LIMIT_SD_Pin GPIO_PIN_14
#define LIMIT_SD_GPIO_Port GPIOE
#define LIMIT_SC_Pin GPIO_PIN_15
#define LIMIT_SC_GPIO_Port GPIOE
#define GPS_RX_Pin GPIO_PIN_10
#define GPS_RX_GPIO_Port GPIOB
#define GPS_TX_Pin GPIO_PIN_11
#define GPS_TX_GPIO_Port GPIOB
#define RACK_EXTEND_B__Pin GPIO_PIN_12
#define RACK_EXTEND_B__GPIO_Port GPIOB
#define RACK_EXTEND_A__Pin GPIO_PIN_13
#define RACK_EXTEND_A__GPIO_Port GPIOB
#define LINEAR_SC__Pin GPIO_PIN_14
#define LINEAR_SC__GPIO_Port GPIOB
#define LINEAR_SC_B15_Pin GPIO_PIN_15
#define LINEAR_SC_B15_GPIO_Port GPIOB
#define LIMIT_SB_Pin GPIO_PIN_8
#define LIMIT_SB_GPIO_Port GPIOD
#define LIMIT_SA_Pin GPIO_PIN_9
#define LIMIT_SA_GPIO_Port GPIOD
#define LIMIT_CD_Pin GPIO_PIN_10
#define LIMIT_CD_GPIO_Port GPIOD
#define LIMIT_CC_Pin GPIO_PIN_11
#define LIMIT_CC_GPIO_Port GPIOD
#define ENC_4A_Pin GPIO_PIN_12
#define ENC_4A_GPIO_Port GPIOD
#define ENC_4B_Pin GPIO_PIN_13
#define ENC_4B_GPIO_Port GPIOD
#define LIMIT_CB_Pin GPIO_PIN_14
#define LIMIT_CB_GPIO_Port GPIOD
#define LIMIT_CA_Pin GPIO_PIN_15
#define LIMIT_CA_GPIO_Port GPIOD
#define ENC_3A_Pin GPIO_PIN_6
#define ENC_3A_GPIO_Port GPIOC
#define ENC_3B_Pin GPIO_PIN_7
#define ENC_3B_GPIO_Port GPIOC
#define SIDE_IR_Pin GPIO_PIN_8
#define SIDE_IR_GPIO_Port GPIOC
#define ADC_I2C3_SDA_Pin GPIO_PIN_9
#define ADC_I2C3_SDA_GPIO_Port GPIOC
#define ADC_I2C3_SCL_Pin GPIO_PIN_8
#define ADC_I2C3_SCL_GPIO_Port GPIOA
#define REAR_IR_FAR_Pin GPIO_PIN_9
#define REAR_IR_FAR_GPIO_Port GPIOA
#define REAR_IR_NEAR_Pin GPIO_PIN_10
#define REAR_IR_NEAR_GPIO_Port GPIOA
#define BASE_8_MOTORA__Pin GPIO_PIN_11
#define BASE_8_MOTORA__GPIO_Port GPIOA
#define BASE_8_MOTORB__Pin GPIO_PIN_12
#define BASE_8_MOTORB__GPIO_Port GPIOA
#define ENC_2A_Pin GPIO_PIN_15
#define ENC_2A_GPIO_Port GPIOA
#define LINEAR_CA__Pin GPIO_PIN_11
#define LINEAR_CA__GPIO_Port GPIOC
#define LINEAR_CA_C12_Pin GPIO_PIN_12
#define LINEAR_CA_C12_GPIO_Port GPIOC
#define LINEAR_CB__Pin GPIO_PIN_0
#define LINEAR_CB__GPIO_Port GPIOD
#define LINEAR_CB_D1_Pin GPIO_PIN_1
#define LINEAR_CB_D1_GPIO_Port GPIOD
#define LINEAR_CC__Pin GPIO_PIN_2
#define LINEAR_CC__GPIO_Port GPIOD
#define LINEAR_CC_D3_Pin GPIO_PIN_3
#define LINEAR_CC_D3_GPIO_Port GPIOD
#define NRF_TX_Pin GPIO_PIN_5
#define NRF_TX_GPIO_Port GPIOD
#define NRF_RX_Pin GPIO_PIN_6
#define NRF_RX_GPIO_Port GPIOD
#define USER_RESET_Pin GPIO_PIN_7
#define USER_RESET_GPIO_Port GPIOD
#define ENC_2B_Pin GPIO_PIN_3
#define ENC_2B_GPIO_Port GPIOB
#define LINEAR_CD__Pin GPIO_PIN_4
#define LINEAR_CD__GPIO_Port GPIOB
#define LINEAR_CD_B5_Pin GPIO_PIN_5
#define LINEAR_CD_B5_GPIO_Port GPIOB
#define WIFI_TX_Pin GPIO_PIN_6
#define WIFI_TX_GPIO_Port GPIOB
#define WIFI_RX_Pin GPIO_PIN_7
#define WIFI_RX_GPIO_Port GPIOB
#define IMU_I2C1_SCL_Pin GPIO_PIN_8
#define IMU_I2C1_SCL_GPIO_Port GPIOB
#define IMU_I2C1_SDA_Pin GPIO_PIN_9
#define IMU_I2C1_SDA_GPIO_Port GPIOB
#define LINEAR_SA__Pin GPIO_PIN_0
#define LINEAR_SA__GPIO_Port GPIOE
#define LINEAR_SA_E1_Pin GPIO_PIN_1
#define LINEAR_SA_E1_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
