/*
  WriteMultipleFields
  
  Description: Writes values to fields 1,2,3,4 and status in a single ThingSpeak update every 20 seconds.
  
  Hardware: ESP8266 based boards
  
  !!! IMPORTANT - Modify the secrets.h file for this project with your network connection and ThingSpeak channel details. !!!
  
  Note:
  - Requires ESP8266WiFi library and ESP8622 board add-on. See https://github.com/esp8266/Arduino for details.
  - Select the target hardware from the Tools->Board menu
  - This example is written for a network using WPA encryption. For WEP or WPA, change the WiFi.begin() call accordingly.
  
  ThingSpeak ( https://www.thingspeak.com ) is an analytic IoT platform service that allows you to aggregate, visualize, and 
  analyze live data streams in the cloud. Visit https://www.thingspeak.com to sign up for a free account and create a channel.  
  
  Documentation for the ThingSpeak Communication Library for Arduino is in the README.md folder where the library was installed.
  See https://www.mathworks.com/help/thingspeak/index.html for the full ThingSpeak documentation.
  
  For licensing information, see the accompanying license file.
  
  Copyright 2018, The MathWorks, Inc.
*/

#include "ThingSpeak.h"
#include "secrets.h"
#include <ESP8266WiFi.h>

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

// Initialize our values
int number1 = 0;
int number2 = random(0,100);
int number3 = random(0,100);
int number4 = random(0,100);

char temp[34];
String tempy;

void setup() {
  Serial.begin(115200);  // Initialize serial

  WiFi.mode(WIFI_STA); 
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

void loop() {

  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }


    while(Serial.read()!='$');
    delay(10);
    for (int i=0; i<34; i++)
     {
      
      temp[i] = Serial.read();
      delay(3);
     }
    //Serial.println(temp);
    
  
  // set the status
  ThingSpeak.setStatus(temp);
  Serial.println(temp);

 tempy = temp[0];
 tempy.concat(temp[1]);
 tempy.concat(temp[2]);
 ThingSpeak.setField(1, tempy);
 tempy = temp[3];
 tempy.concat(temp[4]);
 tempy.concat(temp[5]);
 ThingSpeak.setField(2, tempy);
 tempy = temp[6];
 tempy.concat(temp[7]);
 tempy.concat(temp[8]);
 ThingSpeak.setField(3, tempy);
 tempy = temp[9];
 tempy.concat(temp[10]);
 tempy.concat(temp[11]);
 tempy.concat(temp[12]);
 tempy.concat(temp[13]);
 ThingSpeak.setField(4, tempy);
 tempy = temp[14];
 tempy.concat(temp[15]);
 tempy.concat(temp[16]);
 tempy.concat(temp[17]);
 tempy.concat(temp[18]);
 tempy.concat(temp[19]);
 ThingSpeak.setField(5, tempy);
 tempy = temp[20];
 tempy.concat(temp[21]);
 tempy.concat(temp[22]);
 tempy.concat(temp[23]);
 tempy.concat(temp[24]);
 ThingSpeak.setField(6, tempy);
 tempy = temp[25];
 tempy.concat(temp[26]);
 tempy.concat(temp[27]);
 ThingSpeak.setField(7, tempy);
 tempy = temp[28];
 tempy.concat(temp[29]);
 tempy.concat(temp[30]);
 tempy.concat(temp[31]);
 tempy.concat(temp[32]);
 ThingSpeak.setField(8, tempy);
 
  
  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  
  //delay(20000); // Wait 20 seconds to update the channel again
}
